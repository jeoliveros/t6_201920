package controller;


import model.logic.MVCModelo;
import view.MVCView;
import model.logic.*;

import java.util.ArrayList;
import java.util.Scanner;
import model.logic.*;
import view.*;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();

		modelo = new MVCModelo();
	}

	public void run() 
	{

		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		


		while( !fin ){
			view.printMenu();


			int option = lector.nextInt();
			switch(option){

			case 1:


				view.printMessage("Se empezaron a cargar los datos");

				String resp = modelo.leerDatosJson();
				
				
			
				
				view.printMessage(resp+"\n");
				
				
				
				break;

			case 2:

				
				view.printMessage("digite el movementId");

				int Movement = lector.nextInt();
				
				ZonasUber ZonaEncontrada =  modelo.R2_ConsultarUnaZonaPorId(Movement);
				
				String nombreZona = ZonaEncontrada.getScanombre();
				double perimetro = ZonaEncontrada.getShape_leng();
				double area = ZonaEncontrada.getShape_area();
				int numeropuntos= ZonaEncontrada.getArreglopuntos().size() ;
				
				
				view.printMessage("El nombre de la zona es: " +nombreZona+"\n"+ "-----------\n"+
								  "El perimetro de la zona es:" + perimetro+"\n" +  "-----------\n"+
								  "El area de la zona es: " +area+"\n"+  "-----------\n"+
								  "El numero de puntos de la zona es:" + numeropuntos+"\n" );
				//Mostrar el nombre de la zona, su perímetro, su área y el número de puntos (coordenadas geográficas) que definen el perímetro de la zona.

				

				break;

			case 3:

				view.printMessage("digite el rango del movement superior");

				int MovementSuperior = lector.nextInt();
				
				
				view.printMessage("digite el rango del movement Inferior");

				int MovementInferior = lector.nextInt();


				// Las zonas deben mostrarse de menor a mayor MOVEMENT_ID.
				
			   ArrayList<ZonasUber> resultados = modelo.R3_ConsultarLasZonasEnUnRangoEspeci­fico(MovementInferior, MovementSuperior);
				
			 
				
				 for (int i = 0; i < resultados.size(); i++) {
					 
					 ZonasUber temporal = resultados.get(i);
					 
					 
						String nombreZona1 = temporal.getScanombre();
						
						double perimetro1 = temporal.getShape_leng();
						
						double area1 = temporal.getShape_area();
						
						int numeropuntos1= temporal.getArreglopuntos().size() ;
						
					 view.printMessage("\n"+i+" ---------------------------------------------\n"+ "El nombre de la zona es: " +nombreZona1+"\n"+ "-----------\n"+
							 "el perimetro de la zona es:" + perimetro1+"\n" + "-----------\n"+
							 "El area de la zona es: " +area1+"\n"+ "-----------\n"+
							 "el numero de puntos de la zona es:" + numeropuntos1+"\n" );
					
				}
				 
				 
				break;




			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}
	}
}




