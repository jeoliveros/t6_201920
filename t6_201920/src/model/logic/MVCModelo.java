package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.CredentialException;

import model.logic.ZonasUber;
import model.logic.Punto;

import com.google.gson.*;

import model.data_structures.ArbolBalanceadoRojoNegro;
import model.data_structures.Nodo;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo <T extends Comparable<T>>
{

	public final static String DATOS_JSON = "./data/bogota_cadastral.json";

	private ArbolBalanceadoRojoNegro arbolbalanceadoL;


	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{


		arbolbalanceadoL = new ArbolBalanceadoRojoNegro<>();

	}








	public static Object lecturaDePrueba() throws IOException, Exception
	{




		//PRUEBA------------------------------------

		File f = new File("./data/bogota_cadastral.json");
		BufferedReader bf = new BufferedReader(new FileReader(f));

		String json = "";
		String line = "";
		while(line != null)
		{
			json += line;
			line = bf.readLine();
		}

		bf.close();

		Gson gson = new Gson();

		Object datos = gson.fromJson(json, Object.class);

		return datos;

		//------------------------------------------
	}










	public String leerDatosJson()
	{
		int contador = 0;

		int MOVEMENT_IDMAximo= 0;
		int MOVEMENT_IDMinimo= 999999999;
		
		try {


			//Leemos el archivo
			FileReader archivo = new FileReader(DATOS_JSON);
			BufferedReader lector = new BufferedReader(archivo);


			JsonElement elementoPrincipal = new JsonParser().parse(lector);


			//obtenemos cada zaona
			JsonObject obj = elementoPrincipal.getAsJsonObject();


			JsonArray listafeatures = obj.getAsJsonArray("features");


			//Buscamos la informacion de cada zona
			for (int i = 0; i < listafeatures.size(); i++)
			{


				JsonObject ZONA = listafeatures.get(i).getAsJsonObject();

				if(ZONA == null)
					continue;

				
				JsonObject geometria = ZONA.getAsJsonObject("geometry");

				if(geometria == null)
					continue;
				
				JsonArray cordenadas = geometria.getAsJsonArray("coordinates");
				
				if(cordenadas == null)
					continue;

				//System.out.println("Alguno llega después del continue");
				
				cordenadas = cordenadas.get(0).getAsJsonArray();
				cordenadas = cordenadas.get(0).getAsJsonArray();



				//inicializamos la lista
				JsonArray listapuntosZona = cordenadas; /* = new Punto[cordenadas.size()];*/

				/*
				
				//No hay memoria suficiente para cargar puntos individualmente
				
				//cargamos cada punto a la lista de puntos.
				for (int j = 0; j < cordenadas.size(); j++) 
				{
					JsonArray longitudYLatitud = cordenadas.get(j).getAsJsonArray();

					double longitud = longitudYLatitud.get(0).getAsDouble();
					double latitud = longitudYLatitud.get(1).getAsDouble();

					//creo punto

					Punto nuevoPunto = new Punto(longitud, latitud);

					listapuntosZona[i] = nuevoPunto;
				}
				*/
				JsonObject propiedades = ZONA.getAsJsonObject("properties");

				int pcartodb_id =  propiedades.get("cartodb_id").getAsInt();
				int pscacodigo =propiedades.get("scacodigo").getAsInt();
				int pscatipo =propiedades.get("scatipo").getAsInt();
				String pscanombre = propiedades.get("scanombre").getAsString();
				double pshape_leng = propiedades.get("shape_leng").getAsDouble();
				double pshape_area = propiedades.get("shape_area").getAsDouble();
				int pMOVEMENT_ID = propiedades.get("MOVEMENT_ID").getAsInt();

				ZonasUber a = new ZonasUber(listapuntosZona, pcartodb_id, pscacodigo, pscatipo, pscanombre, pshape_leng, pshape_area, pMOVEMENT_ID);

				contador++;
				System.out.println("La " + i + "-ésima zona es " + a);	

				if(a.getMOVEMENT_ID()> MOVEMENT_IDMAximo  )
				{
					MOVEMENT_IDMAximo= a.getMOVEMENT_ID();
				}
				
				if( a.getMOVEMENT_ID()< MOVEMENT_IDMinimo )
				{
					MOVEMENT_IDMinimo = a.getMOVEMENT_ID();
				}
				
				arbolbalanceadoL.put(pMOVEMENT_ID, a);
				
				
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	

		//System.out.println(arbolbalanceadoL.darAltura());
		//System.out.println(arbolbalanceadoL.altura());
		
		return ("\n"+ "El numero toal de zonas es: "+ contador + "\n"+ "-----------\n"+ "El valor minimo de MOVEMENT_ID es: "+MOVEMENT_IDMinimo+"\n" +"-----------\n"+ "El valor maximo de MOVEMENT_ID es: "+ MOVEMENT_IDMAximo);

		
		
	}












	/**
	 * Dado un MOVEMENT_ID de una zona que ingresa el usuario se busca su informaciÃ³n asociada en el Ã�rbol Balanceado RedBlackBST construido.
	 * Mostrar el nombre de la zona, su perÃ­metro, su Ã¡rea y el nÃºmero de puntos (coordenadas geogrÃ¡ficas) que definen el perÃ­metro de la zona.
	 * @param movementId
	 */
	public ZonasUber  R2_ConsultarUnaZonaPorId(int movementId)
	{

		return (ZonasUber) arbolbalanceadoL.get(movementId);
	}

	/**
	 * Dado un rango de MOVEMENT_IDs (mov_id_inferior y mov_id_maximo) que ingresa el usuario se buscan todas las zonas en el rango. 
	 * De cada zona se muestra su MOVEMENT_ID, nombre, perÃ­metro, Ã¡rea y el nÃºmero de puntos (coordenadas geogrÃ¡ficas) que conforman el perÃ­metro de la zona.
	 * Las zonas deben mostrarse de menor a mayor MOVEMENT_ID.
	 * 
	 * @param MovementInferior
	 * @param MovementSuperior
	 */
	public ArrayList<ZonasUber>   R3_ConsultarLasZonasEnUnRangoEspeci­fico (int MovementInferior, int MovementSuperior)
	{
		int tam = (MovementSuperior+1)-MovementInferior;
		
		ZonasUber[] respuestaConLaszona = new ZonasUber[20];
		
		
		//No se si sea un problema que use un arraylist pero intente usar un arreglo de tipo Zona uber como el de arriba pero no me funciono
		ArrayList<ZonasUber> respuestaConLaszonas = new ArrayList<>();
		
		boolean termino= false;

		

		for (int i = MovementInferior ; i < MovementSuperior+1 ; i++) {

			ZonasUber verificar = R2_ConsultarUnaZonaPorId(i);

			
			
			if( R2_ConsultarUnaZonaPorId(i)!=null )
			{
				//respuestaConLaszonas[i] = verificar;
				respuestaConLaszonas.add(verificar);
			}	
		}
		return respuestaConLaszonas;

	}

}

















