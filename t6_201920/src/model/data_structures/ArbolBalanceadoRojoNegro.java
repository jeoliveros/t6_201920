package model.data_structures;

import java.util.NoSuchElementException;

public class ArbolBalanceadoRojoNegro <Key extends Comparable<Key>, Value> {

	private static final boolean ROJO = true;
	private static final boolean NEGRO = false;
	//Raiz del arbolo binario
	private Nodo raiz;
	private int size;


	/**
	 * Este metodo nos ayuda a verficar el color de un
	 * nodo enlazado con respecto a su padre
	 * @param h es el nodo a donde se apinta 
	 * @return si el color del nodo a donde se apunta es
	 * rojo o no
	 */
	private boolean isRed (Nodo h)
	{
		// Si el nodo recibido por parametro es null devuelve falso 
		if(h== null)
			return false;
		//Si no es nulo se devielve que 
		return h.color==ROJO;
	}
	/**
	 * Se encarga de rotar un enlazado rojo que es
	 * @param h que es 
	 * @return
	 */
	private Nodo rotaIzquierda(Nodo h)
	{
		Nodo hijo = h.dere;
		h.dere = hijo.izq;
		hijo.izq= h;
		hijo.color= h.color;
		h.color= ROJO;
		hijo.N=h.N;
		h.N= 1 + size(h.izq) + size(h.dere);
		return hijo;
	}
	/**
	 * Se encarga de rotar un enlazado rojo que esta
	 * hacia la derecha y quiere ser rotado a la izquierda
	 * para cumplir con los caracterizticas de arboles rojos
	 * negros. Se debe pasar el nodo se�alado a la parte izquierda
	 * y el que se encontraba a la derecha a ser el padre de este nodo 
	 * se�alado
	 * @param h es el se�alado, es el nodo padre del nodo que se encuentra
	 * a la derecha
	 * @return retorna el hijo que antes era la raiz
	 */
	private Nodo rotaDerecha(Nodo h) {
		Nodo hijo = h.izq;
		h.izq= hijo.dere;
		hijo.dere= h;
		h.color= ROJO;
		hijo.N=h.N;
		h.N=1 + size(h.izq) + size(h.dere);
		size();
		return hijo;
	}
	/**Este metodo se encarga de que cuando hay un arbol
	 * binario con uno nodo pap� que tiene dos hijos enlazados con rojo 
	 * cambioamos el color de enlace de los hijos a negro y el papa termina
	 * con un enlace rojo. 
	 * @param h se refiere al papa nodo
	 */
	private void cambiarColores(Nodo h) {
		h.color=ROJO;
		h.izq.color=NEGRO;
		h.dere.color= NEGRO;
	}
	/**Este metodo se encarga de darnos el tama�o de un
	 * arbol balanceado completo  
	 * @return el subArbol, teniendo como parametro un nodo, que queremos que
	 * sea la raiz.
	 */
	public int size() {
		return(size(raiz)); 
	}

	/**
	 * Este metodo nos da todo el tama�o de un subArbolo del arbol completo
	 * esto lo puedes ver parandote en el nodo actual y contando sus hijos y 
	 * hacerlo recursivamente. 
	 * @param nodoAct representa el pap� nodo donde vamos a empezar a trabajar
	 * @return el tama�o del subArbol desde donde nos paramos.
	 */
	public int size(Nodo nodoAct) { 
		if (nodoAct == null) {
			return(0); 
		}
		else { 
			return(size(nodoAct.izq) + 1 + size(nodoAct.dere)); 
		}

	}
	/**
	 * 
	 * @param key
	 * @param val
	 */
	public void put (Key key, Value val) {
		if (key == null) throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) 
		{
			delete(key);
			return;
		}

		raiz = put(raiz, key, val);
		raiz.color = NEGRO;
	}

	/**
	 * Is this symbol table empty?
	 * @return {@code true} if this symbol table is empty and {@code false} otherwise
	 */
	public boolean isEmpty() {
		return raiz== null;
	}

	/**
	 * Removes the smallest key and associated value from the symbol table.
	 * @throws NoSuchElementException if the symbol table is empty
	 */
	public void deleteMin() {
		if (isEmpty()) throw new NoSuchElementException("BST underflow");

		// if both children of root are black, set root to red
		if (!isRed(raiz.izq) && !isRed(raiz.dere))
			raiz.color = ROJO;

		raiz= deleteMin(raiz);
		if (!isEmpty()) raiz.color = NEGRO;
		// assert check();
	}

	// delete the key-value pair with the minimum key rooted at h
	private Nodo deleteMin(Nodo h) { 
		if (h.izq == null)
			return null;

		if (!isRed(h.izq) && !isRed(h.izq.izq))
			h = moveRedLeft(h);

		h.izq = deleteMin(h.izq);
		return balance(h);
	}


	//-----------------------------
	//Funciones que ayudan al arbol Rojo Negro
	//---------------------------------
	// Assuming that h is red and both h.left and h.left.left
	// are black, make h.left or one of its children red.
	private Nodo moveRedLeft(Nodo h) {
		// assert (h != null);
		// assert isRed(h) && !isRed(h.left) && !isRed(h.left.left);

		cambiarColores(h);;
		if (isRed(h.dere.izq)) { 
			h.dere = rotaDerecha(h.dere);
			h = rotaIzquierda(h);
			cambiarColores(h);
		}
		return h;
	}

	private Nodo moveRedRight(Nodo h) {
		// assert (h != null);
		// assert isRed(h) && !isRed(h.right) && !isRed(h.right.left);
		cambiarColores(h);
		if (isRed(h.izq.izq)) { 
			h = rotaDerecha(h);
			cambiarColores(h);
		}
		return h;
	}
	/**
	 * Removes the largest key and associated value from the symbol table.
	 * @throws NoSuchElementException if the symbol table is empty
	 */
	public void deleteMax() {
		if (isEmpty()) throw new NoSuchElementException("BST underflow");

		// if both children of root are black, set root to red
		if (!isRed(raiz.izq) && !isRed(raiz.dere))
			raiz.color = ROJO;

		raiz = deleteMax(raiz);
		if (!isEmpty()) raiz.color = NEGRO;
		// assert check();
	}

	// delete the key-value pair with the maximum key rooted at h
	private Nodo deleteMax(Nodo h) { 
		if (isRed(h.izq))
			h = rotaDerecha(h);

		if (h.dere == null)
			return null;

		if (!isRed(h.dere) && !isRed(h.dere.izq))
			h = moveRedRight(h);

		h.dere = deleteMax(h.dere);

		return balance(h);
	}

	/**
	 * Removes the specified key and its associated value from this symbol table     
	 * (if the key is in this symbol table).    
	 *
	 * @param  key the key
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void delete(Key key) { 
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");
		if (!contains(key)) return;

		// if both children of root are black, set root to red
		if (!isRed(raiz.izq) && !isRed(raiz.dere))
			raiz.color = ROJO;

		raiz = delete(raiz, key);
		if (!isEmpty()) raiz.color = NEGRO;
		// assert check();
	}

	/**
	 * Returns the smallest key in the symbol table.
	 * @return the smallest key in the symbol table
	 * @throws NoSuchElementException if the symbol table is empty
	 */
	public Key min() {
		if (isEmpty()) throw new NoSuchElementException("calls min() with empty symbol table");
		return (Key) min(raiz).llave;
	} 

	// the smallest key in subtree rooted at x; null if no such key
	private Nodo min(Nodo x) { 
		// assert x != null;
		if (x.izq == null) 
			return x; 
		else
			return min(x.izq); 
	}

	// delete the key-value pair with the given key rooted at h
	private Nodo delete(Nodo h, Key key) { 
		// assert get(h, key) != null;

		if (key.compareTo((Key) h.llave) < 0)  {
			if (!isRed(h.izq) && !isRed(h.izq.izq))
				h = moveRedLeft(h);
			h.izq = delete(h.izq, key);
		}
		else {
			if (isRed(h.izq))
				h = rotaDerecha(h);
			if (key.compareTo((Key) h.llave) == 0 && (h.dere == null))
				return null;
			if (!isRed(h.dere) && !isRed(h.dere.izq))
				h = moveRedRight(h);
			if (key.compareTo((Key) h.llave) == 0) {
				Nodo x = min(h.dere);
				h.llave = x.llave;
				h.valor = x.valor;
				// h.val = get(h.right, min(h.right).key);
				// h.key = min(h.right).key;
				h.dere = deleteMin(h.dere);
			}
			else h.dere = delete(h.dere, key);
		}
		return balance(h);
	}

	private Nodo put(Nodo h, Key llave, Value valor) { 
		if (h == null) {
			return new Nodo(llave, valor, ROJO, 1);
		}
		int cmp = llave.compareTo((Key) h.llave);
		if      (cmp < 0) h.izq  = put(h.izq,  llave, valor); 
		else if (cmp > 0) h.dere = put(h.dere, llave, valor); 
		else              h.valor   = valor;

		// fix-up any right-leaning links
		if (isRed(h.dere) && !isRed(h.izq)) h = rotaIzquierda(h);
		if (isRed(h.izq)  &&  isRed(h.izq)) h = rotaDerecha(h);
		if (isRed(h.izq)  &&  isRed(h.dere))     cambiarColores(h);
		h.N = size(h.izq) + size(h.dere) + 1;

		return h;
	}

	/**
	 * Returns the value associated with the given key.
	 * @param key the key
	 * @return the value associated with the given key if the key is in the symbol table
	 *     and {@code null} if the key is not in the symbol table
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public Value get(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to get() is null");

		return get(raiz, key);
	}

	// value associated with the given key in subtree rooted at x; null if no such key
	private Value get(Nodo x, Key key) {



		if(x == null) {
			return null;
		}

		if(x.darLlave().compareTo(key) == 0) {
			//Las llaves son iguales
			//Es el valor buscado
			return (Value) x.darValor();
		}
		else if(x.darLlave().compareTo(key) < 0) {
			//Como x es menor Key vamos a la derecha
			return get(x.NodoDerecha(), key);		
		}
		else
		{
			//Como no es ni igual ni mayor, debe ser menor
			//Se va a la izquierda
			return get(x.NodoIzquierda(), key);
		}
	}
	/**
	 * Se vuelve a balancear el arbol
	 */
	// restore red-black tree invariant
	private Nodo balance(Nodo h) {
		// assert (h != null);
		int tamanoNodo= h.size();
		if (isRed(h.dere))                      h = rotaIzquierda(h);
		if (isRed(h.izq) && isRed(h.izq.izq)) h = rotaDerecha(h);
		if (isRed(h.izq) && isRed(h.dere))     cambiarColores(h);

		tamanoNodo = size(h.izq) + size(h.dere) + 1;
		return h;
	}
	/**
	 * Does this symbol table contain the given key?
	 * @param key the key
	 * @return {@code true} if this symbol table contains {@code key} and
	 *     {@code false} otherwise
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public boolean contains(Key key) {
		return get(key) != null;
	}

	
	
	
	public int altura()
	{
		if(raiz==null)
		{
			return 0;
		}
		
		return height(raiz)-1;
	}
	
	public int height( Nodo node)
	{
		if(node==null)
		{
			return 0;
		}
		
		int izquierda = height(node.izq);
		
		int derecha = height(node.dere);
		
		if(izquierda> derecha )
		{
			return izquierda;
		}
		return derecha;
	}
	
	
	public int darAltura ()
	{
		return maxDepth(raiz);
	}

	public int maxDepth(Nodo node)  
    { 
        if (node == null) 
            return 0; 
        else 
        { 
            /* compute the depth of each subtree */
            int lDepth = maxDepth(node.izq); 
            int rDepth = maxDepth(node.dere); 
   
            /* use the larger one */
            if (lDepth > rDepth) 
                return (lDepth + 1); 
             else 
                return (rDepth + 1); 
        } 
    } 
//	{        
//		if( esHoja( ) )  
//		{          
//			return 1;
//		}   
//		else        
//		{            
//			int a1 = ( raiz.izq == null ) ? 0 : darAltura( );            
//			int a2 = ( raiz.dere == null ) ? 0 : darAltura( );  
//			
//			return 1 + Math.max( a1, a2 );        
//		}
//	}
//
//	public boolean esHoja( )
//	{
//		return raiz.izq  == null && raiz.dere == null;
//	}





}


